'use strict';

class Vector {

  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  plus(vector) {
    if (!(vector instanceof Vector)) {
      throw new TypeError('Можно прибавлять к вектору только вектор типа Vector');
    }

    return new Vector(this.x + vector.x, this.y + vector.y);
  }

  times(factor) {
    if (!(typeof factor === 'number')) {
      throw new TypeError('Множитель для координат вектора должен быть числом');
    }

    return new Vector(this.x * factor, this.y * factor);
  }

}


class Actor {

  constructor(pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
    if (!(pos instanceof Vector && size instanceof Vector && speed instanceof Vector)) {
      throw new TypeError('Параметры pos, size и speed конструктора Actor должны быть типа Vector');
    }

    this.pos = pos;
    this.size = size;
    this.speed = speed;
  }

  act() {}

  get type() {
    return 'actor';
  }

  get left() {
    return this.pos.x;
  }

  get top() {
    return this.pos.y;
  }

  get right() {
    return this.pos.x + this.size.x;
  }

  get bottom() {
    return this.pos.y + this.size.y;
  }

  isIntersect(otherActor) {
    if (!(otherActor instanceof Actor)) {
      throw new TypeError('Параметр метода isIntersect должен быть типа Actor');
    }

    if (otherActor === this) {
      return false;
    }

    return !(this.top >= otherActor.bottom || this.bottom <= otherActor.top || this.right <= otherActor.left || this.left >= otherActor.right);

  }

}

class Level {

  constructor(grid, actors) {
    this.grid = grid || [];
    this.actors = actors || [];
    this.player = this.actors.find(item => item.type === 'player');
    this.status = null;
    this.finishDelay = 1;
  }

  get height() {
    return this.grid.length;
  }

  get width() {
    return this.grid.reduce((a, b) => Math.max(a, b.length), 0);
  }

  isFinished() {
    return this.status !== null && this.finishDelay < 0;
  }

  actorAt(actor) {
    if (!(actor instanceof Actor)) {
      throw new TypeError('Параметр actor метода actorAt класса Level должен быть типа Actor');
    }
    return this.actors.find(item => item.isIntersect(actor));
  }

  obstacleAt(pos, size) {
    if (!(pos instanceof Vector && size instanceof Vector)) {
      throw new TypeError('Параметры pos и size метода obstacleAt класса Level должны быть типа Vector');
    }

    const posAndSizeY = pos.y + size.y,
          posAndSizeX = pos.x + size.x,
          left = Math.floor(Math.min(pos.x, posAndSizeX)),
          right = Math.ceil(Math.max(pos.x, posAndSizeX)),
          top = Math.floor(Math.min(pos.y, posAndSizeY)),
          bottom = Math.ceil(Math.max(pos.y, posAndSizeY));

    if (bottom > this.height) {
      return 'lava';
    }

    if (left < 0 || top < 0 || right > this.width) {
      return 'wall';
    }

    for (let y = top; y < bottom; ++y) {
      for (let x = left; x < right; ++x) {
        if (this.grid[y][x] !== undefined) {
          return this.grid[y][x];
        }
      }
    }
  }

  removeActor(actor) {
    if (!(actor instanceof Actor)) {
      throw new TypeError('Параметр actor метода removeActor класса Level должен быть типа Actor');
    }
    const index = this.actors.indexOf(actor);
    if (index !== -1) this.actors.splice(index, 1);
  }

  noMoreActors(type) {
    if (type === undefined) {
      return !this.actors.length;
    }
    if (typeof type !== 'string') {
      throw new TypeError('Параметр type метода noMoreActors класса Level должен быть типа string или undefined');
    }
    return !this.actors.filter(item => item.type === type).length;
  }

  playerTouched(type, actor) {

    if (typeof type !== 'string') {
      throw new TypeError('Параметр type метода playerTouched класса Level должен быть типа string');
    }

    if (actor !== undefined && !(actor instanceof Actor)) {
      throw new TypeError('Параметр actor метода playerTouched класса Level должен быть типа Actor');
    }

    if (this.status !== null) {
      return;
    }

    if (type === 'lava' || type === 'fireball') {
      this.status = 'lost';
      return;
    }

    if (type === 'coin') {
      this.removeActor(actor);
      if (this.noMoreActors('coin')) {
        this.status = 'won';
        return;
      }
    }

  }

}


class LevelParser {

  constructor(fabrics) {
    this.fabrics = fabrics;
  }

  actorFromSymbol(symbol) {
    if (symbol !== undefined && this.fabrics) {
      return this.fabrics[symbol];
    }
  }

  obstacleFromSymbol(symbol) {
    const obstacles = { 'x': 'wall', '!': 'lava' };
    
    if (symbol !== undefined) {
      return obstacles[symbol];
    }
  }

  createGrid(strings) {
    if (!(strings instanceof Array)) {
      throw new TypeError('Параметр strings метода createGrid класса LevelParser должен быть массивом строк');
    }

    const result = [];

    strings.forEach(entry => {
      if (typeof entry !== 'string') {
        throw new TypeError('Параметр strings метода createGrid класса LevelParser должен быть массивом строк');
      }
      result.push([...entry].map(symbol => this.obstacleFromSymbol(symbol)));
    });

    return result;
  }

  createActors(strings) {
    if (!(strings instanceof Array)) {
      throw new TypeError('Параметр strings метода createActors класса LevelParser должен быть массивом строк');
    }

    const result = [];

    strings.forEach((entry, y) => {
      if (typeof entry !== 'string') {
        throw new TypeError('Параметр strings метода createActors класса LevelParser должен быть массивом строк');
      }
      [...entry].forEach((symbol, x) => {
        const fabric = this.actorFromSymbol(symbol);
        if (typeof fabric === 'function') {
          const obj = new fabric(new Vector(x, y));
          if (obj instanceof Actor) {
            result.push(obj);
          }          
        }
      });
    });

    return result;
  }

  parse(strings) {
    return new Level(this.createGrid(strings), this.createActors(strings));
  }

}

class Fireball extends Actor {

  constructor(pos = new Vector(0, 0), speed = new Vector(0, 0)) {
    super(pos, new Vector(1, 1), speed);
  }

  get type() {
    return 'fireball';
  }

  getNextPosition(time = 1) {
    if (typeof time !== 'number') {
      throw new TypeError('Параметр time функции getNextPosition класса Fireball должен быть числом');
    }

    return new Vector(this.pos.x + time * this.speed.x, this.pos.y + time * this.speed.y)
  }

  handleObstacle() {
    this.speed = this.speed.times(-1);
  }

  act(time, level) {
    if (typeof time !== 'number') {
      throw new TypeError('Параметр time функции act класса Fireball должен быть числом');
    }

    const nextPos = this.getNextPosition(time);
    if (level.obstacleAt(nextPos, this.size)) {
      this.handleObstacle();
    } else {
      this.pos = nextPos;
    }
  }

}

class HorizontalFireball extends Fireball {

  constructor(pos = new Vector(0, 0)) {
    super(pos, new Vector(2, 0));
  }

}

class VerticalFireball extends Fireball {

  constructor(pos = new Vector(0, 0)) {
    super(pos, new Vector(0, 2));
  }

}

class FireRain extends Fireball {

  constructor(pos = new Vector(0, 0)) {
    super(pos, new Vector(0, 3));
    this.startPos = pos;
  }

  handleObstacle() {
    this.pos = this.startPos;
  }

}

class Coin extends Actor {

  constructor(pos = new Vector(0, 0)) {
    pos = pos.plus(new Vector(0.2, 0.1));
    super(pos, new Vector(0.6, 0.6), new Vector(0, 0));
    this.spring = Math.random() * 2 * Math.PI;
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.startPos = pos;
  }

  get type() {
    return 'coin';
  }

  updateSpring(time = 1) {
    if (typeof time !== 'number') {
      throw new TypeError('Параметр time функции updateSpring класса Coin должен быть числом');
    }

    this.spring += this.springSpeed * time;
  }

  getSpringVector() {
    return new Vector(0, Math.sin(this.spring) * this.springDist);
  }

  getNextPosition(time = 1) {
    if (typeof time !== 'number') {
      throw new TypeError('Параметр time функции getNextPosition класса Coin должен быть числом');
    }
    this.updateSpring(time);
    return this.startPos.plus(this.getSpringVector());
  }

  act(time) {
    if (typeof time !== 'number') {
      throw new TypeError('Параметр time функции act класса Coin должен быть числом');
    }

    this.pos = this.getNextPosition(time);
  }

}


class Player extends Actor {

  constructor(pos = new Vector(0, 0)) {
    super(pos.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5), new Vector(0, 0));
  }

  get type() {
    return 'player';
  }

}

const schemas = [
  [
    "     v                 ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "  |xxx       w         ",
    "  o                 o  ",
    "  x               = x  ",
    "  x          o o    x  ",
    "  x  @    *  xxxxx  x  ",
    "  xxxxx             x  ",
    "      x!!!!!!!!!!!!!x  ",
    "      xxxxxxxxxxxxxxx  ",
    "                       "
  ],
  [
    "     v                 ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "  |                    ",
    "  o                 o  ",
    "  x               = x  ",
    "  x          o o    x  ",
    "  x  @       xxxxx  x  ",
    "  xxxxx             x  ",
    "      x!!!!!!!!!!!!!x  ",
    "      xxxxxxxxxxxxxxx  ",
    "                       "
  ],
  [
    "        |           |  ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "                       ",
    "     |                 ",
    "                       ",
    "         =      |      ",
    " @ |  o            o   ",
    "xxxxxxxxx!!!!!!!xxxxxxx",
    "                       "
  ],
  [
    "                       ",
    "                       ",
    "                       ",
    "    o                  ",
    "    x      | x!!x=     ",
    "         x             ",
    "                      x",
    "                       ",
    "                       ",
    "                       ",
    "               xxx     ",
    "                       ",
    "                       ",
    "       xxx  |          ",
    "                       ",
    " @                     ",
    "xxx                    ",
    "                       "
  ], [
    "   v         v",
    "              ",
    "         !o!  ",
    "              ",
    "              ",
    "              ",
    "              ",
    "         xxx  ",
    "          o   ",
    "        =     ",
    "  @           ",
    "  xxxx        ",
    "  |           ",
    "      xxx    x",
    "              ",
    "          !   ",
    "              ",
    "              ",
    " o       x    ",
    " x      x     ",
    "       x      ",
    "      x       ",
    "   xx         ",
    "              "
  ]
];
const actorDict = {
  '@': Player,
  'v': FireRain,
  'o': Coin,
  '=': HorizontalFireball,
  '|': VerticalFireball
}
const parser = new LevelParser(actorDict);
runGame(schemas, parser, DOMDisplay)
  .then(() => console.log('Вы выиграли приз!'));

